# models

The *models* package contains all the modules related to the logic models. They are all based on *pydantic*'s *BaseModel*.