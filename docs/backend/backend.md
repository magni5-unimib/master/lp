# Backend

The *Backend* component is in charge of managing the *model* of the application and dealing with the *business logic*.

It is accessible by clients via API. It receives HTTP requests from the *Frontend*, it performs business logic procedures and it sends JSON responses.

The logical operations could require accessing to the data stored in the *Database*. CRUD actions are performed using the *motor* driver against a *MongoDB* istance hosted on *AWS*. BSON responses are parsed and again processed by the application.

![Backend architectural spike](../resources/backend.jpg "Backend architectural spike")