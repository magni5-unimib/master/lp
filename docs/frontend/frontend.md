# Frontend

The *Frontend* component is in charge of managing the *view* of the application and dealing with the *presentation logic*.

It is accessed by a user via web browser. It handles the interactions and it makes HTPP requests to the *Backend*. It then receives JSON responses and updates the view presented to the user by the client.

![Frontend architectural spike](../resources/frontend.jpg "Frontend architectural spike")