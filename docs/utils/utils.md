# Utils

The *Utils* directory contains scripts and other pieces of software that are generally related to the repository and could be useful in the future, but that are not strictly included in the application source code.