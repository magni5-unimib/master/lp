import React from "react";
import { render, screen } from "@testing-library/react";
import ProfileDetail from "../../components/ProfileDetail";
import "@testing-library/jest-dom";

// Mock the onClose function
const onCloseMock = jest.fn();

// Sample album data for testing
const sampleUser = {
  username: "TestUser11",
  //password: "test_Psw",
  name: "Test_name",
  surname: "test_surname-01-01",
};

describe("ProfileDetail Component - Unit Tests", () => {
  test("renders user details", () => {
    render(<ProfileDetail user={sampleUser} onClose={onCloseMock} />);

    // Check if album details are rendered
    expect(screen.getByText("TestUser11")).toBeInTheDocument();
    //expect(screen.getByText("Test_name")).toBeInTheDocument();
    //expect(screen.getByText("test_surname-01-01")).toBeInTheDocument();
  });
});
