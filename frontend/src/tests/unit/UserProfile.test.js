import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import axios from "axios";
import UserProfile from "../../components/UserProfile";

jest.mock("axios");

describe("UserProfile Component", () => {
  const mockedUserData = {
    username: 'testUser',
    profile_picture: 'mockedBase64Image',
    followers: [],
    followed: [],
  };

  const mockedUserData2 = {
    username: 'testUser',
    profile_picture: 'mockedBase64Image',
    followers: [],
    followed: [],
  };

  it("fetches and displays user profile data", async () => {
    axios.get.mockResolvedValueOnce({ data: mockedUserData });
    axios.get.mockResolvedValueOnce({ data: mockedUserData2 });
    render(<UserProfile userData={{}} toggleSidebar={() => {}} />);

    // Expect loading state
    expect(screen.getByTestId('loading-indicator')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-indicator')).not.toBeInTheDocument();
    });

    expect(screen.getByTestId('user-profile-image')).toBeInTheDocument();
    expect(screen.getByText(mockedUserData.username)).toBeInTheDocument();
    expect(screen.getByText('Followers')).toBeInTheDocument();
    expect(screen.getByText('Followed')).toBeInTheDocument();
  });

  it("renders error message if user data is not found", async () => {
    axios.get.mockRejectedValueOnce(new Error('User data not found'));
    axios.get.mockResolvedValueOnce({ data: mockedUserData2 });
    
    render(<UserProfile userData={{}} toggleSidebar={() => {}} />);

    // Expect loading state
    expect(screen.getByTestId('loading-indicator')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-indicator')).not.toBeInTheDocument();
    });

    expect(screen.getByText('There was an error fetching the user data!')).toBeInTheDocument();
  });

  it("hides the follow button if theuser is not logged", async () => {
    axios.get.mockRejectedValueOnce(new Error('User data not found'));
    axios.get.mockResolvedValueOnce({ data: mockedUserData2 });
    render(<UserProfile userData={{}} toggleSidebar={() => {}} />);

    // Expect loading state
    expect(screen.getByTestId('loading-indicator')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-indicator')).not.toBeInTheDocument();
    });

    expect(screen.queryByTestId('user-profile-follow-button')).not.toBeInTheDocument();
  });
});
