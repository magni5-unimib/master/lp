import React from "react";
import { render, screen } from "@testing-library/react";
import ProfileCardListView from "../../components/ProfileCardListView";

describe("ProfileCardListView Component", () => {
  it("renders without crashing", () => {
    const userList = [
      {
        user_id: 1,
        username: "TestUser",
        //password: "test_Psw",
        name: "Test_name",
        surname: "test_surname-01-01",
        email: "test@test.it", // Dummy tracks
      },
      {
        user_id: 2,
        username: "TestUser1",
        //password: "test_Psw",
        name: "Test_name",
        surname: "test_surname-01-01",
        email: "test@test.it", // Dummy tracks
      },
    ];

    render(<ProfileCardListView userList={userList} />);
  });

  it("renders all users in the list", () => {
    const userList = [
      {
        //user_id: 1,
        username: "TestUser",
        //password: "test_Psw",
        name: "Test_name",
        surname: "test_surname-01-01",
        email: "test@test.it", // Dummy tracks
      },
      {
        //user_id: 2,
        username: "TestUser",
        //password: "test_Psw",
        name: "Test_name",
        surname: "test_surname-01-01",
        email: "test@test.it", // Dummy tracks
      },
    ];

    render(<ProfileCardListView userList={userList} />);

    const userCards = screen.getAllByTestId("user-card");

    expect(userCards).toHaveLength(userList.length);
  });
});
