import React from "react";
import { render, screen } from "@testing-library/react";
import ProfileCard from "../../components/ProfileCard";
import "@testing-library/jest-dom/extend-expect";

describe("ProfileCard Component", () => {
  // Unit Tests
  describe("Unit Tests", () => {
    it("renders user name correctly", () => {
      const user = {
        user_id: 1,
        username: "TestUser3",
        //password: "test_Psw",
        name: "Test_name",
        surname: "test_surname-01-01",
        email: "test@test.it", // Dummy tracks
      };

      render(<ProfileCard user={user} />);
      expect(screen.getByText(user.username)).toBeInTheDocument();
    });
  });
});
