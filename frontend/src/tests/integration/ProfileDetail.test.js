import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ProfileDetail from "../../components/ProfileDetail";

// Mock the onClose function
const onCloseMock = jest.fn();

const sampleUser = {
  username: "TestUser3",
  //password: "test_Psw",
  name: "Test_name",
  surname: "test_surname-01-01",
};

describe("ProfileDetail Component - Integration Tests", () => {
  test("calls onClose when clicking outside the component", () => {
    render(<ProfileDetail user={sampleUser} onClose={onCloseMock} />);

    // Simulate click outside the component
    fireEvent.click(document.body);

    // Check if onClose is called
    expect(onCloseMock).toHaveBeenCalled();
  });

  test("does not call onClose when clicking inside the component", () => {
    render(<ProfileDetail user={sampleUser} onClose={onCloseMock} />);

    // Simulate click inside the component
    fireEvent.click(screen.getByText("TestUser3"));

    // Check if onClose is not called
    expect(onCloseMock).not.toHaveBeenCalled();
  });
});
