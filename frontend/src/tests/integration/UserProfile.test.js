import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import axios from "axios";
import UserProfile from "../../components/UserProfile";

jest.mock("axios");

describe("UserProfile Integration Tests", () => {
  const mockedUserData = {
    username: "testUser",
    profile_picture: "mockedBase64Image",
    followers: [],
    followed: [],
  };

  const mockedUserData2 = {
    username: 'testUser',
    profile_picture: 'mockedBase64Image',
    followers: [],
    followed: [],
  };

  const mockedToggleSidebar = jest.fn();

  it("fetches and displays user profile data and interacts with Navbar", async () => {
    axios.get.mockResolvedValueOnce({ data: mockedUserData });
    axios.get.mockResolvedValueOnce({ data: mockedUserData2 });


    render(<UserProfile userData={{}} toggleSidebar={mockedToggleSidebar} />);

    // Expect loading state
    expect(screen.getByTestId("loading-indicator")).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByTestId("loading-indicator")).not.toBeInTheDocument();
    });

    // Check if user profile data is displayed
    expect(screen.getByTestId("user-profile-image")).toBeInTheDocument();
    expect(screen.getByText(mockedUserData.username)).toBeInTheDocument();
    expect(screen.getByText("Followers")).toBeInTheDocument();
    expect(screen.getByText("Followed")).toBeInTheDocument();

    // Simulate interaction with Navbar
    const toggleButton = screen.getByTestId("sidebar-toggle");
    fireEvent.click(toggleButton);
    expect(mockedToggleSidebar).toHaveBeenCalled();
  });

  it("renders error message if user data is not found and Navbar interaction works", async () => {
    axios.get.mockRejectedValueOnce(new Error("User data not found"));
    axios.get.mockResolvedValueOnce({ data: mockedUserData });

    render(<UserProfile userData={{}} toggleSidebar={mockedToggleSidebar} />);

    // Expect loading state
    expect(screen.getByTestId("loading-indicator")).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.queryByTestId("loading-indicator")).not.toBeInTheDocument();
    });

    // Check if error message is displayed
    expect(
      screen.getByText("There was an error fetching the user data!")
    ).toBeInTheDocument();

    // Simulate interaction with Navbar
    const toggleButton = screen.getByTestId("sidebar-toggle");
    fireEvent.click(toggleButton);
    expect(mockedToggleSidebar).toHaveBeenCalled();
  });
});
