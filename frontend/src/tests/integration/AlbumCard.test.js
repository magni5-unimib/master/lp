import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import AlbumCard from "../../components/AlbumCard";
import "@testing-library/jest-dom/extend-expect";

describe("AlbumCard Component", () => {
  // Integration Tests
  describe("Integration Tests", () => {
    it("calls handleClick when clicked", () => {
      const handleClick = jest.spyOn(console, "log");

      const album = {
        album_id: 1,
        album_name: "Test Album",
        album_artists: ["Artist 1", "Artist 2"],
        album_cover_url: "test_url",
        album_release_date: "2024-01-01",
        tracks: [{}, {}],
      };

      render(<AlbumCard album={album} onClick={handleClick} />);
      const albumCard = screen.getByTestId("album-card");
      albumCard.addEventListener("click", () => console.log("Clicked"));
      fireEvent.click(albumCard);
      expect(handleClick).toHaveBeenCalled();
    });
  });
});
