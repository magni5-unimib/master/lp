import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import ProfileCard from "../../components/ProfileCard";
import "@testing-library/jest-dom/extend-expect";

describe("ProfileCard Component", () => {
  // Integration Tests
  describe("Integration Tests", () => {
    it("calls handleClick when clicked", () => {
      const handleClick = jest.spyOn(console, "log");

      const user = {
        user_id: 1,
        username: "TestUser11",
        //password: "test_Psw",
        name: "Test_name",
        surname: "test_surname-01-01",
        //email: "test@test.it",
      };

      render(<ProfileCard user={user} onClick={handleClick} />);
      const userCard = screen.getByTestId("user-card");
      userCard.addEventListener("click", () => console.log("Clicked"));
      fireEvent.click(userCard);
      expect(handleClick).toHaveBeenCalled();
    });
  });
});
