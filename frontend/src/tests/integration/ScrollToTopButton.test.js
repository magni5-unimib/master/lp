import React from "react";
import { render, screen } from "@testing-library/react";
import ScrollToTopButton from "../../components/ScrollToTopButton";
import "@testing-library/jest-dom";

describe("ScrollToTopButton Component - Integration Tests", () => {
  it("renders the button", () => {
    render(<ScrollToTopButton />);
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument();
  });

  it("renders the button icon", () => {
    render(<ScrollToTopButton />);
    const icon = screen.getByTestId("arrow-up-icon");
    expect(icon).toBeInTheDocument();
  });
});
