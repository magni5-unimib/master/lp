import "./App.css";
import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import ScrollToTopButton from "./components/ScrollToTopButton";
import Home from "./components/Home";
import Sidebar from "./components/Sidebar";
import Registration from "./components/Registration";
import DirectChatPage from "./components/DirectChatPage";
import Login from "./components/Login";
import UserProfile from "./components/UserProfile";
import UserSettings from "./components/UserSettings";
import { jwtDecode } from "jwt-decode";

function App() {
  // toggle sidebar visibility
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  // toggle sidebar visibility
  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  //user logic
  const [user, setUser] = useState();
  const [userData, setUserData] = useState(null);
  let token = localStorage.getItem("token");

  useEffect(() => {
    const decodeToken = (token) => {
      if (token) {
        try {
          const decoded = jwtDecode(token);
          return decoded;
        } catch (error) {
          console.error("Error decoding token:", error);
          return null;
        }
      }
    };

    const userDataFromToken = decodeToken(token);
    setUserData(userDataFromToken);
    console.log(userDataFromToken);
  }, [token]);

  //scelta del componente da caricare
  let Component;
  if (window.location.pathname.startsWith("/Registration")) {
    Component = Registration;
  } else if (window.location.pathname.startsWith("/Login")) {
    Component = Login;
  } else if (window.location.pathname.startsWith("/UserProfile")) {
    Component = UserProfile;
  } else if (window.location.pathname.startsWith("/UserSettings")) {
    Component = UserSettings;
  } else if (window.location.pathname.startsWith("/DirectChatPage")) {
    Component = DirectChatPage;
  } else {
    Component = Home;
  }

  if (!user && Component === Login) {
    return (
      <div data-testid="app" className="App">
        <Sidebar isOpen={isSidebarOpen} onClose={toggleSidebar} />
        <div
          className={isSidebarOpen ? "overlay" : ""}
          onClick={toggleSidebar}
        ></div>
        <div className="background"></div>
        <div className="background-color"></div>
        <Login onAuth={(user) => setUser(user)} toggleSidebar={toggleSidebar} />
      </div>
    );
    // } else if (user) {
    //   return (
    //     <DirectChatPage
    //       user={user}
    //       // projectID="04cb1ab7-46bf-4a35-b51a-b2ecd080eaba"
    //       // userName={user}
    //       // userSecret="pass1234"
    //     />
    //   );
  } else if (Component !== Login) {
    return (
      <div data-testid="app" className="App">
        <Sidebar
          isOpen={isSidebarOpen}
          onClose={toggleSidebar}
          userData={userData}
        />
        <div
          className={isSidebarOpen ? "overlay" : ""}
          onClick={toggleSidebar}
        ></div>
        <div className="background"></div>
        <div className="background-color"></div>
        <Component toggleSidebar={toggleSidebar} userData={userData} />
        {Component === Home && <ScrollToTopButton />}
      </div>
    );
  }
}

export default App;
