import React, { useState, useEffect } from "react";
import axios from "axios";
import Navbar from "./Navbar";
import AlbumCardListView from "./AlbumCardListView";
import "./Home.css";
import ProfileCardListView from "./ProfileCardListView";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

function Home({ toggleSidebar, userData }) {
  // album list setup
  const [albumList, setAlbumList] = useState([]);
  const [userList, setUserList] = useState([]);
  const [, setFilter] = useState("");

  // fetch albums informations
  const fetchAlbums = (filter = "") => {
    setFilter(filter);
    axios
      .get(`${BACKEND_URL}/api/albums${filter ? `?fltr=${filter}` : ""}`)
      .then((res) => {
        setAlbumList(res.data);
        setUserList([]);
      })
      .catch((error) => {
        console.error("Error fetching albums:", error);
      });
  };

  // fetch users informations
  const fetchUser = (filter = "") => {
    setFilter(filter);
    axios
      .get(`${BACKEND_URL}/api/users${filter ? `?fltr=${filter}` : ""}`)
      .then((res) => {
        setUserList(res.data);
        setAlbumList([]);
      })
      .catch((error) => {
        console.error("Error fetching user:", error);
      });
  };

  // initial album reading
  useEffect(() => {
    fetchAlbums();
    fetchUser();
  }, []);

  return (
    <div data-testid="home" className="Home">
      <Navbar
        onSearchUser={fetchUser}
        onSearch={fetchAlbums}
        toggleSidebar={toggleSidebar}
        isSearchBarVisible={true}
        userData={userData}
      />
      {albumList.length > 0 && <AlbumCardListView albumList={albumList} />}
      {userList.length > 0 && <ProfileCardListView userList={userList} />}
    </div>
  );
}

export default Home;
