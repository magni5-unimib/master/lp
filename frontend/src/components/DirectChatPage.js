import React, { useState } from "react";
import appLogo from "../img/appLogo.png";
import { jwtDecode } from "jwt-decode";
import { ChatEngine, getOrCreateChat } from "react-chat-engine";
import "./DirectChatPage.css";

const DirectChatPage = (props) => {
  const [username, setUsername] = useState("");

  function createDirectChat(creds) {
    getOrCreateChat(
      creds,
      { is_direct_chat: true, usernames: [username] },
      () => setUsername("")
    );
  }

  const redirectToHomepage = () => {
    window.location.href = "/"; // Redirect to the homepage
  };

  function renderChatForm(creds) {
    return (
      <div className="chat-form">
        <input
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <button onClick={() => createDirectChat(creds)}>Create</button>
      </div>
    );
  }

  const decodeToken = () => {
    let token = localStorage.getItem("token");
    let decoded = "";
    try {
      decoded = jwtDecode(token);
      console.log("User data from token testR", decoded);

      return { username: decoded.username, password: decoded.password };
    } catch (error) {
      console.error("Error decoding token:", error);
      return null;
    }
  };

  const userData = decodeToken();
  let username2 = "";
  let password = "";

  if (userData) {
    username2 = userData.username;
    password = userData.password;
  }

  return (
    <div className="direct-chat-page">
      <div className="homepage-link">
        <a href="/" onClick={redirectToHomepage}>
          <img
            src={appLogo}
            alt="Homepage"
            className="homepage-image"
            onClick={redirectToHomepage} //redirect
          />
        </a>
      </div>
      {/* <div className="chat-engine-container"> */}
      <ChatEngine
        // height="100vh"
        userName={username2} //{props.user.username}
        userSecret={password} //{props.user.password}
        projectID="2d375319-95c1-4481-931f-a800d4b33278"
        renderNewChatForm={(creds) => renderChatForm(creds)}
      />
      {/* </div> */}
    </div>
  );
};
//     <div style={{ height: "100vh", width: "100vw" }}>
//       <a href="/" onClick={redirectToHomepage}>
//         <img
//           src={appLogo}
//           alt="Homepage"
//           className="homepage-image"
//           onClick={redirectToHomepage} //redirect
//         />
//       </a>
//       <div style={{ overflow: "hidden" }}>
//         <ChatEngine
//           height="100vh"
//           userName={username2} //{props.user.username}
//           userSecret={password} //{props.user.password}
//           projectID="2d375319-95c1-4481-931f-a800d4b33278"
//           renderNewChatForm={(creds) => renderChatForm(creds)}
//         />
//       </div>
//     </div>
//   );
// };

export default DirectChatPage;
