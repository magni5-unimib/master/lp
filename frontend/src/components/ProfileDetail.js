import React, { useEffect, useRef, useState } from "react";
import "./ProfileDetail.css";

function ProfileDetail(props) {
  const [isMounted, setIsMounted] = useState(false);
  const profileDetailRef = useRef(null); // Ref for ProfileDetail container

  useEffect(() => {
    setIsMounted(true); // Set isMounted to true when the component mounts

    // Clean up when the component unmounts
    return () => {
      setIsMounted(false);
    };
  }, []);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        isMounted &&
        profileDetailRef.current &&
        !profileDetailRef.current.contains(event.target)
      ) {
        props.onClose();
      }
    };

    // Add event listener when the component is mounted and visible
    if (isMounted) {
      document.addEventListener("click", handleClickOutside);
    }

    // Clean up the event listener when the component unmounts or is no longer visible
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [isMounted, props]);

  return (
    <div className="user-detail-container" ref={profileDetailRef}>
      {/* <div className="album-cover-img">
        <img src={props.user.album_cover_url} alt={props.album.album_name} />
      </div> */}
      <div className="user-infos">
        <h2>{props.user.username}</h2>
        <p>Username {props.user.username}</p>
        <p>Name {props.user.name}</p>
        <p>Surname {props.user.surname}</p>
        {/* <div className="tracks-list">
          <h3>Tracks:</h3>
          <ul>
            {props.album.tracks.map((track, index) => (
              <li key={index}>
                {track.track_name} - {
                  Math.floor(Math.floor(track.track_duration / 1000) / 60)
                }:{
                  (Math.floor(track.track_duration / 1000) % 60).toString().padStart(2, '0')
                }              
              </li>
            ))}
          </ul>
        </div> */}
      </div>
    </div>
  );
}

export default ProfileDetail;
