import React from "react";

const DirectChatButton = ({ onDirectChat }) => {
  let token = localStorage.getItem("token");

  if (token) {
    return (
      <div>
        <button onClick={onDirectChat}>DM's</button>
      </div>
    );
  } else return <div></div>;
};

export default DirectChatButton;
