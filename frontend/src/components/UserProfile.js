import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";
import "./UserProfile.css";
import Navbar from "./Navbar";
import missingUserImage from "../img/missingUserImage.png";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

const UserProfile = ({ userData, toggleSidebar }) => {
  const [fetchedUserData, setFetchedUserData] = useState(null);
  const [fetchedLoggedUserData, setLoggedFetchedUserData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [isFollowed, setIsFollowed] = useState(false)

  let username =
    window.location.pathname.split("/")[
      window.location.pathname.split("/").length - 1
    ];

  const fetchUserData = useCallback(() => {
    axios
      .get(`${BACKEND_URL}/user/${username}`)
      .then((response) => {
        setFetchedUserData(response.data);
        setLoading(false);
      })
      .catch((error) => {
        setError("There was an error fetching the user data!");
        setLoading(false);
      });
  }, [username]);

  let loggedUsername = userData ? userData.username : ''
  const fetchLoggedUserData = useCallback(() => {
    if (userData) {
      axios
      .get(`${BACKEND_URL}/user/${loggedUsername}`)
      .then((response) => {
        setLoggedFetchedUserData(response.data);
        if (response.data.followed.includes(username)) {
          setIsFollowed(true);
        }
      })
      .catch((error) => {
        setError("There was an error fetching the logged user data!");
        setLoading(false);
      });
    } else {
      setFetchedUserData('')
      setLoading(false);
    }
  }, [loggedUsername, userData]);

  const handleFollowUnfollowUser = async (e) => {
    let updatedUserData = fetchedUserData;
    let newFollower = updatedUserData.followers
    if (isFollowed) {
      newFollower.pop(fetchedLoggedUserData.username);
    } else {
      newFollower.push(fetchedLoggedUserData.username);
    }
    updatedUserData.followers = newFollower;
    try {
      axios
        .put(`${BACKEND_URL}/user/${fetchedUserData.username}`, updatedUserData, {
          headers: {
            "Content-Type": "application/json"
          },
        })
        .then(() => {
          setIsFollowed(!isFollowed);
          setFetchedUserData(updatedUserData);
        });
    } catch (error) {
      console.error("Error updating user data:", error.response.data);
    }
  };
  
  const handleFollowUnfollowLoggedUser = async (e) => {
    e.preventDefault();
    let updatedLoggedUserData = fetchedLoggedUserData;
    let newFollowed = updatedLoggedUserData.followed;
    if (isFollowed) {
      newFollowed.pop(fetchedUserData.username);
    } else {
      newFollowed.push(fetchedUserData.username);
    }
    updatedLoggedUserData.followed = newFollowed;
    try {
      axios
        .put(`${BACKEND_URL}/user/${fetchedLoggedUserData.username}`, updatedLoggedUserData, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then(() => {
          handleFollowUnfollowUser();
        });

      console.log("User data updated successfully");

      // Optionally, you can update the local state or perform any other actions after updating user data
    } catch (error) {
      console.error("Error updating user data:", error.response.data);
    }
  };

  useEffect(() => {
    fetchUserData();
  }, [fetchUserData]);

  useEffect(() => {
    fetchLoggedUserData();
  }, [fetchLoggedUserData]);

  if (loading) {
    return (
      <div className="user-profile-container" data-testid="loading-indicator">
        Loading...
      </div>
    );
  }

  if (error) {
    return (
      <div className="user-profile-container">
        <Navbar
          onSearch={null}
          toggleSidebar={toggleSidebar}
          isSearchBarVisible={false}
          userData={userData}
        />
        {error}
      </div>
    );
  }

  if (fetchedUserData) {
    return (
      <div className="user-profile-container">
        <Navbar
          onSearch={null}
          toggleSidebar={toggleSidebar}
          isSearchBarVisible={false}
          userData={userData}
        />
        <div className="user-profile-header">
          <div className="user-profile-photo-username">
            <img
              src={
                (fetchedUserData &&
                  "data:image/jpeg;base64," +
                    fetchedUserData.profile_picture) ||
                missingUserImage
              }
              alt="User.jpg"
              data-testid="user-profile-image"
              className="user-profile-profile-picture"
            />
          </div>
          <div className="user-profile-followers-followed">
            <div className="user-profile-user-info">
              <h4>{fetchedUserData.followers.length}</h4>
              <h4>Followers</h4>
            </div>
            <div className="user-profile-user-info">
              <h4>{fetchedUserData.followed.length}</h4>
              <h4>Followed</h4>
            </div>
          </div>
        </div>
        <h3 className="user-profile-username">{fetchedUserData.username}</h3>
        {fetchedUserData &&
          userData &&
          userData.username !== fetchedUserData.username && (
            <div className="user-profile-button-div">
              <button
                className="user-profile-follow-button"
              data-testid="user-profile-follow-button"
              onClick={handleFollowUnfollowLoggedUser}
              >
              {isFollowed ? "Unfollow" : "Follow"}
              </button>
            </div>
          )}
      </div>
    );
  }

  return null;
};

export default UserProfile;
