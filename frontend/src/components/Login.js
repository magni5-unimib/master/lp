import React, { useState } from "react";
import axios from "axios";
import "./Login.css";
import appLogo from "../img/appLogo.png";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("");
  //const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState("");

  //const { setToken } = useContext(AuthContext);

  const handleLogin = async () => {
    try {
      const response = await axios.post(BACKEND_URL + '/api/login', {
        username: username,
        password: password,
      });

      const token = response.data.token;
      localStorage.setItem("token", token);
      setToken(token);

      console.log("Login successful" + token);
      window.location.href = "/Home";
    } catch (error) {
      console.error("Login error:", error.message);
      if (error.response && error.response.status === 401) {
        setError(
          "Invalid Credentials. Please try again."
        );
      } else {
        setError("An error occurred. Please try again later.");
      }
    }
  };

  const redirectToHomepage = () => {
    window.location.href = "/"; // Redirect to the homepage
  };

  return (
    <div className="login-container">
      <a href="/" onClick={redirectToHomepage}>
        <img
          src={appLogo}
          alt="Homepage"
          className="homepage-image"
          onClick={redirectToHomepage}
        />
      </a>
      <h1 className="login-header">Login</h1>
      <div classname="login-form">
        <input
          type="text"
          className="form-input"
          onChange={(event) => setUsername(event.target.value)}
          placeholder="Username"
          value={username}
          id="username"
          />
        <input
          type={"password"}
          className="form-input"
          onChange={(event) => setPassword(event.target.value)}
          placeholder="Password"
          value={password}
          id="password"
        />
        <button className="submit-button" onClick={handleLogin}>
          Login
        </button>
      </div>   
      {error && <p className="error-message">{error}</p>}
    </div>
  );
}

export default Login;
