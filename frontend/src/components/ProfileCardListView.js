import React, { useState } from "react";
import ProfileCard from "./ProfileCard";
import ProfileDetail from "./ProfileDetail";

function ProfileCardListView(props) {
  const [selectedUser, setSelectedUser] = useState(null);

  const handleUserClick = (user) => {
    /*setSelectedUser(user);*/
    window.location.href = "/UserProfile/" + user.username;
  };

  const handleCloseProfileDetail = () => {
    setSelectedUser(null); // Set selectedAlbum to null to close the AlbumDetail component
  };

  return (
    <div>
      {selectedUser ? (
        <ProfileDetail user={selectedUser} onClose={handleCloseProfileDetail} />
      ) : (
        <ul>
          {props.userList.map((user) => (
            <ProfileCard
              key={user.username}
              user={user}
              onClick={() => handleUserClick(user)}
            />
          ))}
        </ul>
      )}
    </div>
  );
}

export default ProfileCardListView;
