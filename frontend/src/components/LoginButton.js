import React from "react";

const LoginButton = ({ onLogin }) => {
  let token = localStorage.getItem("token");

  if (!token) {
    return (
      <div>
        <button onClick={onLogin}>Login</button>
      </div>
    );
  } else return <div></div>;
};

export default LoginButton;
