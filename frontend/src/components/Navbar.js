import React, { useState, useEffect } from "react";
import appLogo from "../img/appLogo.png";
import missingUserImage from "../img/missingUserImage.png";
import "./Navbar.css";
import { jwtDecode } from "jwt-decode";

function Navbar({ onSearch, onSearchUser, toggleSidebar, isSearchBarVisible, userData }) {
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
    if (searchQuery === "@") {
      onSearch(searchQuery);
    } else if (searchQuery.startsWith("@")) {
      onSearchUser(searchQuery.split("@")[1]);
    } else {
      onSearch(searchQuery);
    }
    //setSearchQuery(e.target.value);
    //onSearch(e.target.value);
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
    }
  };

  const handleProfileClick = () => {
    toggleSidebar();
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light custom-navbar">
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          <img className="appLogo" src={appLogo} alt="Logo" height="50" />
        </a>
        {isSearchBarVisible && (
          <form className="d-flex" onKeyDown={handleKeyDown}>
            <div className="search-container">
              <input
                className="form-control search-input"
                type="search"
                placeholder="Search"
                aria-label="Search"
                value={searchQuery}
                onChange={handleSearch}
                style={{ width: "800px" }}
              />
              <i className="fas fa-search search-icon"></i>
            </div>
          </form>
        )}
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <div
              data-testid="sidebar-toggle"
              className="nav-link"
              onClick={handleProfileClick}
            >
              {userData && <div>Welcome {userData.username}</div>}

              <img
                src={
                  (userData &&
                    "data:image/jpeg;base64," + userData.profile_picture) ||
                  missingUserImage
                }
                alt="User"
                className="user-profile-picture"
              />
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;
