import React from "react";

const SignUpButton = ({ onSignUp }) => {
  let token = localStorage.getItem("token");

  if (!token) {
    return (
      <div>
        <button onClick={onSignUp}>Sign Up</button>
      </div>
    );
  } else return <div></div>;
};

export default SignUpButton;
