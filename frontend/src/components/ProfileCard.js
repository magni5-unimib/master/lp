import React, { useState } from "react";
import "./ProfileCard.css";

function ProfileCard({ user, onClick }) {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleClick = () => {
    if (onClick) {
      onClick(); // Call the onClick function passed from the parent component
    }
  };

  return (
    <div className="user-container">
      <div
        data-testid="user-card"
        key={user.username}
        className={`user-card ${isHovered ? "hovered" : ""}`}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={handleClick}
      >
        {/* <div className="album-cover">
          <img src={album.album_cover_url} alt={album.album_name} />
        </div> */}
        <div className="user-details">
          <h2>{user.username}</h2>
          <p>Name: {user.name}</p>
          <p>Surname: {user.surname}</p>
        </div>
      </div>
    </div>
  );
}
export default ProfileCard;
