import React, { useState, useEffect } from "react";
import { jwtDecode } from "jwt-decode";
import axios from "axios";
import "./UserSettings.css"; // Assicurati di importare il file CSS
import Navbar from "./Navbar";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;
const UserSettings = ({ toggleSidebar }) => {
  //const { token } = useContext(AuthContext);
  //const [token, setToken] = useState("");
  const [userData, setUserData] = useState(null);
  const [editedData, setEditedData] = useState({
    password: "",
    email: "",
    name: "",
    surname: "",
  });

  let token = localStorage.getItem("token");

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedData({ ...editedData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(editedData);
    try {
      await axios.put(`${BACKEND_URL}/user/${userData.username}`, editedData, {
        headers: {
          "Content-Type": "application/json",
          // You may need to include authorization headers if required by your API
          // 'Authorization': `Bearer ${token}`
        },
      });

      console.log("User data updated successfully");
      window.location.href = "/"; // Redirect to the homepage

      // Optionally, you can update the local state or perform any other actions after updating user data
    } catch (error) {
      console.error("Error updating user data:", error.response.data);
    }
  };

  const handleDelete = async () => {
    try {
      await axios.delete(`${BACKEND_URL}/user/${userData.username}`, {
        headers: {
          "Content-Type": "application/json",
        },
      });

      console.log("User deleted successfully");
      localStorage.clear();
      window.location.href = "/";
    } catch (error) {
      console.error("Error deleting user:", error);
    }
  };

  useEffect(() => {
    const decodeToken = (token) => {
      try {
        const decoded = jwtDecode(token);
        return decoded;
      } catch (error) {
        console.error("Error decoding token:", error);
        return null;
      }
    };

    // Move the declaration of userDataFromToken inside the useEffect
    const userDataFromToken = decodeToken(token);
    setUserData(userDataFromToken);
    editedData.password = userDataFromToken.password;
    editedData.email = userDataFromToken.email;
    editedData.name = userDataFromToken.name;
    editedData.surname = userDataFromToken.surname;
    editedData.profile_picture = userDataFromToken.profile_picture;

    console.log(userDataFromToken);
  }, [token, editedData]);

  // You can use the token here for any API calls or other purposes
  return (
    <div className="settings-container">
      <Navbar
        onSearch={null}
        toggleSidebar={toggleSidebar}
        isSearchBarVisible={false}
        userData={userData}
      />
      <h1 className="settings-header">User Profile</h1>
      {userData && (
        <div class="settings-form">
          <div>
            <label htmlFor="name" className="settings-form-label">
              Name:
            </label>
            <input
              type="text"
              className="settings-form-input"
              id="name"
              name="name"
              value={editedData.name || userData.name}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <label htmlFor="surname" className="settings-form-label">
              Surname:
            </label>
            <input
              type="text"
              className="settings-form-input"
              id="surname"
              name="surname"
              value={editedData.surname || userData.surname}
              onChange={handleInputChange}
            />
          </div>
          <div>
            <label htmlFor="email" className="settings-form-label">
              Email:
            </label>
            <input
              type="email"
              className="settings-form-input"
              id="email"
              name="email"
              value={editedData.email || userData.email}
              onChange={handleInputChange}
            />
            <div>
              <label className="settings-form-label">Username:</label>
              <input
                type="text"
                className="settings-form-input"
                id="password"
                name="password"
                placeholder={userData.username}
                disabled
              />
            </div>
            <div>
              <label htmlFor="password" className="settings-form-label">
                Password:
              </label>
              <input
                type="password"
                className="settings-form-input"
                id="password"
                name="password"
                value={editedData.password || userData.password}
                onChange={handleInputChange}
              />
            </div>
            <button
              type="submit"
              className="settings-confirm-button"
              onClick={handleSubmit}
            >
              Save Changes
            </button>
            <button
              type="button"
              className="settings-delete-button"
              onClick={handleDelete}
            >
              Delete User
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default UserSettings;
