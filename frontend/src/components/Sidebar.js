import React, { useEffect, useRef } from "react";
import "./Sidebar.css";

const Sidebar = ({ isOpen, onClose, userData }) => {
  const sidebarRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        isOpen &&
        sidebarRef.current &&
        !sidebarRef.current.contains(event.target)
      ) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isOpen, onClose]);

  const handleLogout = () => {
    localStorage.removeItem("token");
  };

  return (
    isOpen && (
      <div
        data-testid="sidebar"
        ref={sidebarRef}
        className={`sidebar ${isOpen ? "show" : ""}`}
      >
        <ul>
          {localStorage.getItem("token") != null && (
            <li>
              <a href={"/UserProfile/" + userData.username}>Profile</a>
            </li>
          )}
          {localStorage.getItem("token") != null && (
            <li>
              <a href="/UserSettings}>Settings">Settings</a>
            </li>
          )}
          {localStorage.getItem("token") == null && (
            <li>
              <a href="/Registration">Sign Up</a>
            </li>
          )}
          {localStorage.getItem("token") == null && (
            <li>
              <a href="/Login">Login</a>
            </li>
          )}
          {localStorage.getItem("token") != null && (
            <li onClick={handleLogout}>
              <a href="/">Logout</a>
            </li>
          )}
          {localStorage.getItem("token") != null && (
            <li>
              <a href="/DirectChatPage">DMs</a>
            </li>
          )}
        </ul>
      </div>
    )
  );
};

export default Sidebar;
