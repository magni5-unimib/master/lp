# How-to

## Backend

Check [Backend how-to](backend/how-to.md) for component-specific instructions.

## Frontend

Check [Frontend how-to](frontend/how-to.md) for component-specific instructions.

## Docker

If using Docker remember to put the needed environment variables in a ```.env``` file for each application component.

Then execute the following command to setup and run all the components.

```bash
docker compose up
```

Enjoy.