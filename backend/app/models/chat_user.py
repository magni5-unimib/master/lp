'''
Module used to hold all the utilities for the chat user.
'''

from typing import Union
from pydantic import BaseModel


class ChatUser(BaseModel):
    '''
    Class used as a model for the data related to chat users.
    '''

    username: str
    password: str
    email: Union[str, None] = None
    first_name: Union[str, None] = None
    last_name: Union[str, None] = None
