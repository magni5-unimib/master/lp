'''
Module used to hold the information related to the user.
'''

from typing import Optional
from pydantic import BaseModel

class User(BaseModel):
    '''
    Class for the User model: username, password, email, name, surname
    '''
    username: str
    password:str
    email:str
    name:str
    surname:str
    profile_picture: Optional[str]=None
    followers: Optional[list[str]]=[]
    followed: Optional[list[str]]=[]

class UpdateUser(BaseModel):
    '''
    Class for the UpdateModel model: password, email, name, surname
    '''
    username: Optional[str]=""
    password:str
    email:str
    name:str
    surname:str
    profile_picture: Optional[str]=None
    followers: Optional[list[str]]=[]
    followed: Optional[list[str]]=[]
