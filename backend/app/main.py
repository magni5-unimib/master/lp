'''
Module used to manage all the calls at the backend.
'''
import random
import os
import secrets
import base64
import jwt
import uvicorn
import requests
from fastapi import FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware
from dotenv import load_dotenv
from app.models.chat_user import ChatUser
from app.models.user import User
from app.models.user import UpdateUser
from app.database import (
    fetch_one_user_psw,
    fetch_one_user,
    create_user,
    fetch_albums,
    remove_user,
    update_user,
    fetch_users
)

load_dotenv()

app = FastAPI()

origins = [os.environ['FRONTEND_URL']]

app.add_middleware(
    CORSMiddleware,
    allow_origins= origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

SECRET_KEY = secrets.token_hex(32)
ALGORITHM = "HS256"

@app.post("/api/login")
async def login(request: Request):
    '''
    Returns the user who meets the credentials and a Json Web Token is generated.
    Returns: response: a JSON file containing informations about the user found and the JWT.
    '''

    data = await request.json()
    username = data.get("username")
    password = data.get("password")

    response = await fetch_one_user_psw(username, password)
    if response:
        user = await fetch_one_user(username)
        token = jwt.encode({"username": user.get('username'), "password": user.get('password'),
                            "email": user.get('email'),
                            "name": user.get('name'), "surname": user.get('surname'),
                            "profile_picture": user.get('profile_picture')},
                            SECRET_KEY, algorithm=ALGORITHM)
        return {"token": token}
    raise HTTPException(status_code=401, detail="Invalid username or password")

@app.get("/")
def read_root():
    '''
    Returns a set message; It is used to check if the application is reachable.
    Parameters:
        -
    Returns:
        msg (dict[str, str]): the string set to be returned.

    '''
    msg = {"msg": "LP base endpoint"}
    return msg

@app.delete("/user/{username}")
async def delete_user(username):
    '''
    Deletes a user by username
    Parameters:
        username: the username of the user to be deleted
    Returns:
        response: Succesfully deleted or username not found
    '''
    response = await remove_user(username)
    if response:
        return "Successfully deleted item"
    raise HTTPException(404, "Username not found")

@app.put("/user/{username}", response_model= User)
async def modify_user(username, update: UpdateUser):
    '''
    Modifies a user by username
    Parameters:
        username: the username of the user to be deleted
        update: new user data
    Returns:
        response: modified user or username not found
    '''
    response = await update_user(username,update)
    if response:
        return response
    raise HTTPException(404, "not found")

@app.post("/user/", response_model = User)
async def post_user(user:User):
    '''
    Creates a user to the database if the username doesn't already exist
    Parameters:
        user (User): The user to add to the collection
    Returns:
        response: the user added, or error status
    '''
    existing_user = await fetch_one_user(user.username)
    if existing_user:
        raise HTTPException(status_code=400, detail="Username already exists")
    profile_pictures_path = "app/res/profile_pictures/"
    profile_pictures_list = os.listdir(profile_pictures_path)
    selected_profile_picture = random.choice(profile_pictures_list)
    selected_profile_picture_path = os.path.join(profile_pictures_path, selected_profile_picture)
    with open(selected_profile_picture_path, "rb") as f_f:
        profile_picture = f_f.read()
    user.profile_picture = base64.b64encode(profile_picture).decode("utf-8")
    user.followers = []
    user.followed = []
    response = await create_user(user)
    if response:
        return response
    raise HTTPException(400, response)

@app.get("/user/{username}", response_model = User)
async def get_user(username):
    '''
    Returns information about the user sent to the function.
    Parameters:
        username (str): the username of the user to retrieve the information for.
    Returns:
        response: a JSON file containing informations about the user's information.
      '''
    response = await fetch_one_user(username)
    if response:
        return response
    raise HTTPException(400, response)

#if __name__ == "__main__":

#PROJECT_ID = "04cb1ab7-46bf-4a35-b51a-b2ecd080eaba"
#PRIVATE_KEY = "d0f10636-fd7f-4377-be5e-a141c588c74d"
PROJECT_ID ="2d375319-95c1-4481-931f-a800d4b33278"
PRIVATE_KEY ="836bbd5a-d572-45cd-af83-0669a88dd499"

# class User(BaseModel):
#     username: str
#     password: str
#     email: Union[str, None] = None
#     first_name: Union[str, None] = None
#     last_name: Union[str, None] = None

@app.post('/login/')
async def root(user: ChatUser):
    '''
    Returns the chat user who meets the credentials and enables the chat engine.
    Returns: response: a JSON file containing informations about the user found.
    '''
    response = requests.get('https://api.chatengine.io/users/me/',
        headers={
            "Project-ID": PROJECT_ID,
            "User-Name": user.username,
            "User-Secret": user.password
        }, timeout=50
    )
    if response:
        return response.json()
    raise HTTPException(400, response)


@app.get("/api/albums")
async def get_albums(fltr: str = ''):
    '''
    Returns all the albums in the database that satisfy the optional filter.
    Parameters:
        fltr (str): the optional filter to apply to the album database.
    Returns:
        response: a JSON file containing informations about the albums found.
      '''
    response = await fetch_albums(fltr)
    return response

@app.get("/api/users")
async def get_users(fltr: str = ''):
    '''
    Returns all the users in the database that satisfy the optional filter.
    Parameters:
        fltr (str): the optional filter to apply to the user database.
    Returns:
        response: a JSON file containing informations about the users found.
      '''
    response = await fetch_users(fltr)
    return response


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
