'''
Module used to run unit tests on the different endpoint of the application.
'''
import asyncio
import pytest
from httpx import AsyncClient
from app.main import app

@pytest.fixture(scope="module")
def event_loop():
    '''
    Functio used to define custom behaviour of the event loop.
    '''
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()

@pytest.mark.asyncio
async def test_read_default_endpoint():
    '''
    Test to check if the base endpoint of the application is reachable.
    '''
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "LP base endpoint"}

@pytest.mark.asyncio
async def test_get_albums():
    '''
    Test to check if the application correctly retrieves albums object from the database.
    '''
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get("/api/albums")
    assert response.status_code == 200
    assert response.json() != {""}

@pytest.mark.asyncio
async def test_login():
    '''
    Test to check if the application correctly handles login requests.
    '''
    login_data = {"username": "test1", "password": "1234"}
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.post("/api/login", json=login_data)
    assert response.status_code == 200
    assert "token" in response.json()

@pytest.mark.asyncio
async def test_fetch_one_user():
    '''
    Test to check if the application correctly handles user information requests.
    '''
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.get("/user/test1")
    assert response.status_code == 200
    assert response.json() != ""
